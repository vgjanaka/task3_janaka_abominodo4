public class UndoAction implements Action {

    History history;

    public UndoAction(History history){
        this.history = history;
    }

    @Override
    public void execute() {

        history.undo();
    }
}
