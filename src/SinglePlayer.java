public class SinglePlayer extends PlayerObservable {

    public SinglePlayer(PlayerList playerList) {
        this.playerList = playerList;
        this.playerList.addPlayer(this);
    }

    @Override
    public void update() {
        System.out.println("Single Player : " + playerList.getMessage());
    }
}
