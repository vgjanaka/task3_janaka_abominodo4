public abstract  class PlayerObservable {

    protected PlayerList playerList;
    public abstract void update();
}
