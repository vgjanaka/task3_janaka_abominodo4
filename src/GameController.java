public class GameController {

    private Aardvark aardvark;

    public Aardvark startGame() {
        if(aardvark == null){
            aardvark = new Aardvark();
        }
        return aardvark;
    }

    public void playGame(int gamePlayMode) {
        GamePlayer gamePlayer;
        if(gamePlayMode==1){ // normal player
            gamePlayer = new GamePlayer(new NormalPlayer(),aardvark);
        }else{ //auto player
            gamePlayer = new GamePlayer(new AutoPlayer(),aardvark);
        }
        //play game
        gamePlayer.runGame();
    }

    public void exitGame() {
        System.exit(0);
    }

}
