public class RedoAction implements Action {

    History history;

    public RedoAction(History history){
        this.history = history;
    }

    @Override
    public void execute() {
        history.redo();
    }
}
