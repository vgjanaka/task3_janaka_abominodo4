public class History {

    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void undo() {
        System.out.println("History Undo [ Name: " + name);
    }

    public void redo() {
        System.out.println("History Redo[ Name: " + name);
    }
}
