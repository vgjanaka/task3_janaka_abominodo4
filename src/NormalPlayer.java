public class NormalPlayer implements PlayerStrategy {
    @Override
    public void playGame(Aardvark aardvark) {
        IOSpecialist io = new IOSpecialist();
        int runingCun ;
        try {
          String s1 = io.getString();
            runingCun = Integer.parseInt(s1);
        } catch (Exception e) {
            runingCun = -9;
        }
        aardvark.run(runingCun);
    }
}
