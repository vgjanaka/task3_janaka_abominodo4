import java.util.ArrayList;
import java.util.List;

public class AcctionManager {

    List<Action> actionList = new ArrayList<>();

    public void  addAction(Action action){
        actionList.add(action);
    }

    public void runAction(){
        for (Action action :actionList) {
            action.execute();
        }
    }
}

