import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameStartView {
    private JPanel panel1;
    private JTextArea textArea1;
    private JComboBox comboBox1;
    private JButton playButton;
    private JButton exitButton;
    private JTextArea textArea2;
    private JButton redoButton;
    private JButton undoButton;

    private GameController gameController;

    public GameStartView() {

        playButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if(gameController == null){
                    gameController = new GameController();
                }
                gameController.startGame();
                //impl strategy_pattern for player mode
                int playMode = comboBox1.getSelectedIndex();
                gameController.playGame(playMode);
                //add new player
                PlayerList playerList = new PlayerList();
                SinglePlayer singlePlayer = new SinglePlayer(playerList);
                GroupPlayer groupPlayer  = new GroupPlayer(playerList);
                MulityPlayer mulityPlayer  = new MulityPlayer(playerList);
                //send message to all user
                playerList.setMessage("hi all we start game");
                //send message to all user
                playerList.setMessage("we completed 1st level");
                //send message to all user
                playerList.setMessage("game over");
            }
        });
        exitButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                gameController.exitGame();
            }
        });
        undoButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                History history = new History();
                history.setName("Un do Me ");
                UndoAction undoAction = new UndoAction(history);
                AcctionManager acctionManager  = new AcctionManager();
                acctionManager.addAction(undoAction);
                acctionManager.runAction();
            }
        });
        redoButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                History history = new History();
                history.setName("Redo Me ");
                RedoAction redoAction = new RedoAction(history);
                AcctionManager acctionManager  = new AcctionManager();
                acctionManager.addAction(redoAction);
                acctionManager.runAction();
            }
        });
    }
}
