public class GroupPlayer extends PlayerObservable {

    public GroupPlayer(PlayerList playerList) {
        this.playerList = playerList;
        this.playerList.addPlayer(this);
    }

    @Override
    public void update() {
        System.out.println("Group Player : " + playerList.getMessage());
    }
}
