public class GamePlayer {

    PlayerStrategy playerStrategy;
    Aardvark aardvark;

    public GamePlayer(PlayerStrategy playerStrategy, Aardvark aardvark) {
        this.playerStrategy = playerStrategy;
        this.aardvark = aardvark;
    }

    public void runGame() {
        playerStrategy.playGame(aardvark);
    }
}

