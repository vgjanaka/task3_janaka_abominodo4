import java.util.ArrayList;
import java.util.List;

public class PlayerList {

    private List<PlayerObservable> observers = new ArrayList<>();
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String playerName) {
        this.message = playerName;
        notifyAllObservers();
    }

    public void addPlayer(PlayerObservable observer){
        observers.add(observer);
    }

    public void removePlayer(PlayerObservable observer){
        observers.remove(observer);
    }

    public void notifyAllObservers(){
        for (PlayerObservable player : observers) {
            player.update();
        }
    }
}
