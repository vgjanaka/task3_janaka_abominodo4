import java.awt.Dimension;
import java.awt.Graphics;
import java.io.*;
import java.net.InetAddress;
import java.text.DateFormat;
import java.util.*;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class Aardvark {

  private String playerName;
  public List<Domino> dominoList;
  public List<Domino> guessesList;
  public int[][] gridArray = new int[7][8];
  public int[][] guessGridArray = new int[7][8];
  int mode = -1;
  int cf;
  int score;
  long startTime;

  PictureFrame pf = new PictureFrame();

  private void generateDominoes() {
    dominoList = new LinkedList<Domino>();
    int count = 0;
    int x = 0;
    int y = 0;
    for (int l = 0; l <= 6; l++) {
      for (int h = l; h <= 6; h++) {
        Domino d = new Domino(h, l);
        dominoList.add(d);
        d.place(x, y, x + 1, y);
        count++;
        x += 2;
        if (x > 6) {
          x = 0;
          y++;
        }
      }
    }
    if (count != 28) {
      System.out.println("something went wrong generating dominoes");
      System.exit(0);
    }
  }

  private void generateGuesses() {
    guessesList = new LinkedList<Domino>();
    int count = 0;
    int x = 0;
    int y = 0;
    for (int l = 0; l <= 6; l++) {
      for (int h = l; h <= 6; h++) {
        Domino d = new Domino(h, l);
        guessesList.add(d);
        count++;
      }
    }
    if (count != 28) {
      System.out.println("something went wrong generating dominoes");
      System.exit(0);
    }
  }

  void collateGrid() {
    for (Domino d : dominoList) {
      if (!d.placed) {
        gridArray[d.hy][d.hx] = 9;
        gridArray[d.ly][d.lx] = 9;
      } else {
        gridArray[d.hy][d.hx] = d.high;
        gridArray[d.ly][d.lx] = d.low;
      }
    }
  }

  void collateGuessGrid() {
    for (int r = 0; r < 7; r++) {
      for (int c = 0; c < 8; c++) {
        guessGridArray[r][c] = 9;
      }
    }
    for (Domino d : guessesList) {
      if (d.placed) {
        guessGridArray[d.hy][d.hx] = d.high;
        guessGridArray[d.ly][d.lx] = d.low;
      }
    }
  }

  int printGuessGrid(int guessArray [][]) {
    for (int are = 0; are < 7; are++) {
      for (int see = 0; see < 8; see++) {
        if (guessArray[are][see] != 9) {
          System.out.printf("%d", guessGridArray[are][see]);
        } else {
          System.out.print(".");
        }
      }
      System.out.println();
    }
    return 11;
  }

  private void shuffleDominoesOrder() {
    List<Domino> shuffled = new LinkedList<Domino>();

    while (dominoList.size() > 0) {
      int n = (int) (Math.random() * dominoList.size());
      shuffled.add(dominoList.get(n));
      dominoList.remove(n);
    }

    dominoList = shuffled;
  }

  private void invertSomeDominoes() {
    for (Domino d : dominoList) {
      if (Math.random() > 0.5) {
        d.invert();
      }
    }
  }

  private void placeDominoes() {
    int x = 0;
    int y = 0;
    int count = 0;
    for (Domino d : dominoList) {
      count++;
      d.place(x, y, x + 1, y);
      x += 2;
      if (x > 6) {
        x = 0;
        y++;
      }
    }
    if (count != 28) {
      System.out.println("something went wrong generating dominoes");
      System.exit(0);
    }
  }

  private void rotateDominoes() {
    // for (Domino d : dominoes) {
    // if (Math.random() > 0.5) {
    // System.out.println("rotating " + d);
    // }
    // }
    for (int x = 0; x < 7; x++) {
      for (int y = 0; y < 6; y++) {

        tryToRotateDominoAt(x, y);
      }
    }
  }

  private void tryToRotateDominoAt(int x, int y) {
    Domino d = findDominoOrGuessAt(x, y,dominoList);
    if (thisIsTopLeftOfDomino(x, y, d)) {
      if (d.ishl()) {
        boolean weFancyARotation = Math.random() < 0.5;
        if (weFancyARotation) {
          if (theCellBelowIsTopLeftOfHorizontalDomino(x, y)) {
            Domino e = findDominoOrGuessAt(x, y + 1,dominoList);
            e.hx = x;
            e.lx = x;
            d.hx = x + 1;
            d.lx = x + 1;
            e.ly = y + 1;
            e.hy = y;
            d.ly = y + 1;
            d.hy = y;
          }
        }
      } else {
        boolean weFancyARotation = Math.random() < 0.5;
        if (weFancyARotation) {
          if (theCellToTheRightIsTopLeftOfVerticalDomino(x, y)) {
            Domino e = findDominoOrGuessAt(x + 1, y,dominoList);
            e.hx = x;
            e.lx = x + 1;
            d.hx = x;
            d.lx = x + 1;
            e.ly = y + 1;
            e.hy = y + 1;
            d.ly = y;
            d.hy = y;
          }
        }

      }
    }
  }

  private boolean theCellToTheRightIsTopLeftOfVerticalDomino(int x, int y) {
    Domino e = findDominoOrGuessAt(x + 1, y,dominoList);
    return thisIsTopLeftOfDomino(x + 1, y, e) && !e.ishl();
  }

  private boolean theCellBelowIsTopLeftOfHorizontalDomino(int x, int y) {
    Domino e = findDominoOrGuessAt(x, y + 1,dominoList);
    return thisIsTopLeftOfDomino(x, y + 1, e) && e.ishl();
  }

  private boolean thisIsTopLeftOfDomino(int x, int y, Domino d) {
    return (x == Math.min(d.lx, d.hx)) && (y == Math.min(d.ly, d.hy));
  }

  private Domino findDominoOrGuessAt(int x, int y,List<Domino> lisfOfDomino) {
    for (Domino domino : lisfOfDomino) {
      if ((domino.lx == x && domino.ly == y) || (domino.hx == x && domino.hy == y)) {
        return domino;
      }
    }
    return null;
  }


  private Domino findDominoByLH(int x, int y) {
    for (Domino d : dominoList) {
      if ((d.low == x && d.high == y) || (d.high == x && d.low == y)) {
        return d;
      }
    }
    return null;
  }

  private void printDominoesOrGuesses(List<Domino> lisfOfDomino) {
    for (Domino d : lisfOfDomino) {
      System.out.println(d);
    }
  }


  public final int ZERO = 0;

  public void run(int runingCunt) {
    IOSpecialist io = new IOSpecialist();

    System.out
        .println("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe");
    System.out.println("Version 1.0 (c), Kevan Buckley, 2010");
    System.out.println();
    System.out.println(MultiLinugualStringTable.getMessage(0));
    playerName = io.getString();

    System.out.printf("%s %s. %s", MultiLinugualStringTable.getMessage(1),
        playerName, MultiLinugualStringTable.getMessage(2));

//    int runingCunt = -9;
    while (runingCunt != ZERO) {
      System.out.println();
      String h1 = "Main menu";
      String u1 = h1.replaceAll(".", "=");
      System.out.println(u1);
      System.out.println(h1);
      System.out.println(u1);
      System.out.println("1) Play");
      // System.out.println("1) Single player play");
      System.out.println("2) View high scores");
      System.out.println("3) View rules");
      // System.out.println("4) Multiplayer play");
      System.out.println("0) Quit");

//      runingCunt = -9;
//      while (runingCunt == -9) {
//        try {
//          String s1 = io.getString();
//          runingCunt = Integer.parseInt(s1);
//        } catch (Exception e) {
//          runingCunt = -9;
//        }
//      }
      switch (runingCunt) {
      case 0: {
        if (dominoList == null) {
          System.out.println("It is a shame that you did not want to play");
        } else {
          System.out.println("Thankyou for playing");
        }
        System.exit(0);
        break;
      }
      case 1: {
        System.out.println();
        String h4 = "Select difficulty";
        String u4 = h4.replaceAll(".", "=");
        System.out.println(u4);
        System.out.println(h4);
        System.out.println(u4);
        System.out.println("1) Simples");
        System.out.println("2) Not-so-simples");
        System.out.println("3) Super-duper-shuffled");
        int c2 = -7;
        while (!(c2 == 1 || c2 == 2 || c2 == 3)) {
          try {
            String s2 = io.getString();
            c2 = Integer.parseInt(s2);
          } catch (Exception e) {
            c2 = -7;
          }
        }
        switch (c2) {
        case 1:
          generateDominoes();
          shuffleDominoesOrder();
          placeDominoes();
          collateGrid();
          // printGrid();
          break;
        case 2:
          generateDominoes();
          shuffleDominoesOrder();
          placeDominoes();
          rotateDominoes();
          collateGrid();
          // printGrid();
          break;
        default:
          generateDominoes();
          shuffleDominoesOrder();
          placeDominoes();
          rotateDominoes();
          rotateDominoes();
          rotateDominoes();
          invertSomeDominoes();
          collateGrid();
          break;
        }
        printGuessGrid(gridArray);
        generateGuesses();
        collateGuessGrid();
        mode = 1;
        cf = 0;
        score = 0;
        startTime = System.currentTimeMillis();
        pf.PictureFrame(this);
        pf.dp.repaint();
        int c3 = -7;
        while (c3 != ZERO) {
          System.out.println();
          String h5 = "Play menu";
          String u5 = h5.replaceAll(".", "=");
          System.out.println(u5);
          System.out.println(h5);
          System.out.println(u5);
          System.out.println("1) Print the grid");
          System.out.println("2) Print the box");
          System.out.println("3) Print the dominos");
          System.out.println("4) Place a domino");
          System.out.println("5) Unplace a domino");
          System.out.println("6) Get some assistance");
          System.out.println("7) Check your score");
          System.out.println("0) Given up");
          System.out.println("What do you want to do " + playerName + "?");
          c3 = 9;
          // make sure the user enters something valid
          while (!((c3 == 1 || c3 == 2 || c3 == 3)) && (c3 != 4)
              && (c3 != ZERO) && (c3 != 5) && (c3 != 6) && (c3 != 7)) {
            try {
              String s3 = io.getString();
              c3 = Integer.parseInt(s3);
            } catch (Exception e) {
              c3 = gecko(55);
            }
          }
          switch (c3) {
          case 0:

            break;
          case 1:
            printGuessGrid(gridArray);
            break;
          case 2:
            printGuessGrid(guessGridArray);
            break;
          case 3:
            Collections.sort(guessesList);
              printDominoesOrGuesses(guessesList);
            break;
          case 4:
            System.out.println("Where will the top left of the domino be?");
            System.out.println("Column?");

            int x = gecko(99);
            x = getAxisValue(io, x,65,1,8);
            System.out.println("Row?");
            int y = gecko(98);
            y = getAxisValue(io, y,64,1,7);
            x--;
            y--;
            System.out.println("Horizontal or Vertical (H or V)?");
            boolean horiz;
            int y2,
            x2;
            Location lotion;
            while ("AVFC" != "BCFC") {
              String s3 = io.getString();
              if (s3 != null && s3.toUpperCase().startsWith("H")) {
                lotion = new Location(x, y, Location.DIRECTION.HORIZONTAL);
                System.out.println("Direction to place is " + lotion.d);
                horiz = true;
                x2 = x + 1;
                y2 = y;
                break;
              }
              if (s3 != null && s3.toUpperCase().startsWith("V")) {
                horiz = false;
                lotion = new Location(x, y, Location.DIRECTION.VERTICAL);
                System.out.println("Direction to place is " + lotion.d);
                x2 = x;
                y2 = y + 1;
                break;
              }
              System.out.println("Enter H or V");
            }
            if (x2 > 7 || y2 > 6) {
              System.out
                  .println("Problems placing the domino with that position and direction");
            } else {
              // find which domino this could be
              Domino d = findDominoOrGuessAt(gridArray[y][x], gridArray[y2][x2],guessesList);
              if (d == null) {
                System.out.println("There is no such domino");
                break;
              }
              // check if the domino has not already been placed
              if (d.placed) {
                System.out.println("That domino has already been placed :");
                System.out.println(d);
                break;
              }
              // check guessgrid to make sure the space is vacant
              if (guessGridArray[y][x] != 9 || guessGridArray[y2][x2] != 9) {
                System.out.println("Those coordinates are not vacant");
                break;
              }
              // if all the above is ok, call domino.place and updateGuessGrid
              guessGridArray[y][x] = gridArray[y][x];
              guessGridArray[y2][x2] = gridArray[y2][x2];
              if (gridArray[y][x] == d.high && gridArray[y2][x2] == d.low) {
                d.place(x, y, x2, y2);
              } else {
                d.place(x2, y2, x, y);
              }
              score += 1000;
              collateGuessGrid();
              pf.dp.repaint();
            }
            break;
          case 5:
            System.out.println("Enter a position that the domino occupies");
            System.out.println("Column?");

            int x13 = -9;
            x13 = getAxisValueWithOutGecko(io, x13,-7,1,8);
            System.out.println("Row?");
            int y13 = -9;
            y13 = getAxisValueWithOutGecko(io, x13,-7,1,7);
            x13--;
            y13--;
            Domino lkj = findDominoOrGuessAt(x13, y13,guessesList);
            if (lkj == null) {
              System.out.println("Couln't find a domino there");
            } else {
              lkj.placed = false;
              guessGridArray[lkj.hy][lkj.hx] = 9;
              guessGridArray[lkj.ly][lkj.lx] = 9;
              score -= 1000;
              collateGuessGrid();
              pf.dp.repaint();
            }
            break;
          case 7:
            System.out.printf("%s your score is %d\n", playerName, score);
            break;
          case 6:
            System.out.println();
            String h8 = "So you want to cheat, huh?";
            String u8 = h8.replaceAll(".", "=");
            System.out.println(u8);
            System.out.println(h8);
            System.out.println(u8);
            System.out.println("1) Find a particular Domino (costs you 500)");
            System.out.println("2) Which domino is at ... (costs you 500)");
            System.out.println("3) Find all certainties (costs you 2000)");
            System.out.println("4) Find all possibilities (costs you 10000)");
            System.out.println("0) You have changed your mind about cheating");
            System.out.println("What do you want to do?");
            int yy = -9;
            while (yy < 0 || yy > 4) {
              try {
                String s3 = io.getString();
                yy = Integer.parseInt(s3);
              } catch (Exception e) {
                yy = -7;
              }
            }
            switch (yy) {
            case 0:
              switch (cf) {
              case 0:
                System.out.println("Well done");
                System.out.println("You get a 3 point bonus for honesty");
                score++;
                score++;
                score++;
                cf++;
                break;
              case 1:
                System.out
                    .println("So you though you could get the 3 point bonus twice");
                System.out.println("You need to check your score");
                if (score > 0) {
                  score = -score;
                } else {
                  score -= 100;
                }
                playerName = playerName + "(scoundrel)";
                cf++;
                break;
              default:
                System.out.println("Some people just don't learn");
                playerName = playerName.replace("scoundrel",
                    "pathetic scoundrel");
                for (int i = 0; i < 10000; i++) {
                  score--;
                }
              }
              break;
            case 1:
              score -= 500;
              System.out.println("Which domino?");
              System.out.println("Number on one side?");
              int x4 = -9;
              x4 = getAxisValueWithOutGecko(io,x4,-7,0,6);
              System.out.println("Number on the other side?");
              int x5 = -9;
              x5  = getAxisValueWithOutGecko(io,x5,-7,0,6);
              Domino dd = findDominoByLH(x5, x4);
              System.out.println(dd);

              break;
            case 2:
              score -= 500;
              System.out.println("Which location?");
              System.out.println("Column?");
              int x3 = -9;
              x4 = getAxisValueWithOutGecko(io,x3,-7,1,8);
              System.out.println("Row?");
              int y3 = -9;
              y3 = getAxisValueWithOutGecko(io,y3,-7,1,7);
              x3--;
              y3--;
              Domino lkj2 = findDominoOrGuessAt(x3, y3,dominoList);
              System.out.println(lkj2);
              break;
            case 3: {
              score -= 2000;
              HashMap<Domino, List<Location>> map = new HashMap<Domino, List<Location>>();
              populateLocation(map);
              displayLocation(map,true);
              break;
            }
            case 4: {
              score -= 10000;
              HashMap<Domino, List<Location>> map = new HashMap<Domino, List<Location>>();
              populateLocation(map);
              displayLocation(map,false);
              break;
            }
            }
          }

        }
        mode = 0;
        printGuessGrid(gridArray);
        pf.dp.repaint();
        long now = System.currentTimeMillis();
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        int gap = (int) (now - startTime);
        int bonus = 60000 - gap;
        score += bonus > 0 ? bonus / 1000 : 0;
        recordTheScore();
        System.out.println("Here is the solution:");
        System.out.println();
        Collections.sort(dominoList);
        printDominoesOrGuesses(dominoList);
        System.out.println("you scored " + score);

      }
        break;
      case 2: {
        String h4 = "High Scores";
        String u4 = h4.replaceAll(".", "=");
        System.out.println(u4);
        System.out.println(h4);
        System.out.println(u4);

        File f = new File("score.txt");
        if (!(f.exists() && f.isFile() && f.canRead())) {
          System.out.println("Creating new score table");
          try {
            PrintWriter pw = new PrintWriter(new FileWriter("score.txt", true));
            String n = playerName.replaceAll(",", "_");
            pw.print("Hugh Jass");
            pw.print(",");
            pw.print(1500);
            pw.print(",");
            pw.println(1281625395123L);
            pw.print("Ivana Tinkle");
            pw.print(",");
            pw.print(1100);
            pw.print(",");
            pw.println(1281625395123L);
            pw.flush();
            pw.close();
          } catch (Exception e) {
            System.out.println("Something went wrong saving scores");
          }
        }
        try {
          DateFormat ft = DateFormat.getDateInstance(DateFormat.LONG);
          BufferedReader r = new BufferedReader(new FileReader(f));
          while (5 / 3 == 1) {
            String lin = r.readLine();
            if (lin == null || lin.length() == 0)
              break;
            String[] parts = lin.split(",");
            System.out.printf("%20s %6s %s\n", parts[0], parts[1], ft
                .format(new Date(Long.parseLong(parts[2]))));

          }

        } catch (Exception e) {
          System.out.println("Malfunction!!");
          System.exit(0);
        }

      }
        break;

      case 3: {
        String h4 = "Rules";
        String u4 = h4.replaceAll(".", "=");
        System.out.println(u4);
        System.out.println(h4);
        System.out.println(u4);
        System.out.println(h4);

        JFrame f = new JFrame("Dicezy rule are like Yahtzee rules");

        f.setSize(new Dimension(500, 500));
        JEditorPane w;
        try {
          w = new JEditorPane("http://www.scit.wlv.ac.uk/~in6659/abominodo/");

        } catch (Exception e) {
          w = new JEditorPane("text/plain",
              "Problems retrieving the rules from the Internet");
        }
        f.setContentPane(new JScrollPane(w));
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        break;

      }
      case 4:
        System.out
            .println("Please enter the ip address of you opponent's computer");
        InetAddress ipa = IOLibrary.getIPAddress();
        new ConnectionGenius(ipa).fireUpGame();
      }

    }

  }

  private void displayLocation(HashMap<Domino, List<Location>> map, boolean isOneCheck) {
    for (Domino key : map.keySet()) {
      List<Location> locs = map.get(key);
      if(isOneCheck){
        if (locs.size() == 1) {
          Location loc = locs.get(0);
          System.out.printf("[%d%d]", key.high, key.low);
          System.out.println(loc);
        }
      }else{
        System.out.printf("[%d%d]", key.high, key.low);
        for (Location loc : locs) {
          System.out.print(loc);
        }
        System.out.println();
      }
    }
  }

  private void populateLocation(HashMap<Domino, List<Location>> map) {
    for (int r = 0; r < 6; r++) {
      for (int c = 0; c < 7; c++) {
        Domino hd = findDominoOrGuessAt(gridArray[r][c], gridArray[r][c + 1], guessesList);
        Domino vd = findDominoOrGuessAt(gridArray[r][c], gridArray[r + 1][c],guessesList);
        List<Location> l = map.get(hd);
        if (l == null) {
          l = new LinkedList<Location>();
          map.put(hd, l);
        }
        l.add(new Location(r, c));
        l = map.get(vd);
        if (l == null) {
          l = new LinkedList<Location>();
          map.put(vd, l);
        }
        l.add(new Location(r, c));
      }
    }
  }

  private int getAxisValueWithOutGecko(IOSpecialist ioSpecialist, int axisValus,
                   int errorValue, int leftCondtionValue,int rightCondtionValue) {
    while (axisValus < leftCondtionValue || axisValus > rightCondtionValue) {
      try {
        String s3 = ioSpecialist.getString();
        axisValus = Integer.parseInt(s3);
      } catch (Exception e) {
        axisValus = errorValue;
      }
    }
    return axisValus;
  }

  private int getAxisValue(IOSpecialist ioSpecialist, int axisValue,
                           int geckoValue, int leftCondtionValue,int rightCondtionValue) {
    while (axisValue < leftCondtionValue || axisValue > rightCondtionValue) {
      try {
        String s3 = ioSpecialist.getString();
        axisValue = Integer.parseInt(s3);
      } catch (Exception e) {
        System.out.println("Bad input");
        axisValue = gecko(geckoValue);
      }
    }
    return axisValue;
  }

  private void recordTheScore() {
    try {
      PrintWriter pw = new PrintWriter(new FileWriter("score.txt", true));
      String n = playerName.replaceAll(",", "_");
      pw.print(n);
      pw.print(",");
      pw.print(score);
      pw.print(",");
      pw.println(System.currentTimeMillis());
      pw.flush();
      pw.close();
    } catch (Exception e) {
      System.out.println("Something went wrong saving scores");
    }
  }

//  public static void main(String[] args) {
//    new Aardvark().run();
//  }

  public void drawDominoes(Graphics g) {
    for (Domino d : dominoList) {
      pf.dp.drawDomino(g, d);
    }
  }

  public static int gecko(int cont) {
    if (cont == (32 & 16)) {
      return -7;
    } else {
      if (cont < 0) {
        return gecko(cont + 1 | 0);
      } else {
        return gecko(cont - 1 | 0);
      }
    }
  }

  public void drawGuesses(Graphics g) {
    for (Domino d : guessesList) {
      pf.dp.drawDomino(g, d);
    }
  }

}
