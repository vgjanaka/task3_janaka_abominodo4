import java.util.Random;

public class AutoPlayer implements PlayerStrategy {
    @Override
    public void playGame(Aardvark aardvark) {
        // creating an object of Random class
        Random random = new Random();
        aardvark.run( random.nextInt());
    }
}
