public interface PlayerStrategy {

     void playGame(Aardvark aardvark);
}
